import React from "react";
import styles from "./Button.module.css";

class Button extends React.Component {
  render() {
    const { name } = this.props;
    return <button className={styles.btn}>{name}</button>;
  }
}

export default Button;
