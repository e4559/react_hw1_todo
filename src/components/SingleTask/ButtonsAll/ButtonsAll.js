import React from "react";
import styles from "./ButtonsAll.module.css";

class ButtonsAll extends React.Component {
  render() {
    const { handleImportant, handleEdit, edit } = this.props;
    return (
      <div className={styles.btnWrapper}>
        <button onClick={handleEdit}> {edit ? "Edit" : "Save"}</button>
        <button onClick={handleImportant}>Important</button>
        <button>Done</button>
        <button>Delete</button>
      </div>
    );
  }
}

export default ButtonsAll;
