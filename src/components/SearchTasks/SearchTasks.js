import React from "react";
import Title from "../Title";
import Button from "./Button";
import styles from "./SearchTasks.module.css";

class SearchTasks extends React.Component {
  render() {
    return (
      <div className={styles.container}>
        <Title title={<h2>Search</h2>} />
        <div className={styles.inputWrapper}>
          <input className={styles.input} placeholder="Search Task" />
          <Button name="All" />
          <Button name="Important" />
          <Button name="Done" />
        </div>
      </div>
    );
  }
}

export default SearchTasks;
