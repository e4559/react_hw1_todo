import React from "react";
import ButtonsAll from "./ButtonsAll";
import styles from "./SingleTask.module.css";

class SingleTask extends React.Component {
  state = {
    important: false,
    edit: true,
  };
  handleImportant = () => {
    this.setState({ important: !this.state.important });
  };
  handleEdit = () => {
    this.setState({ edit: !this.state.edit });
    this.refFocus.focus();
    // console.log(this.state);
  };

  render() {
    const { task } = this.props;

    const style = {
      color: this.state.important ?  '#6a4f95' : "white ",
    };
    return (
      <div className={styles.singleTask}>
        <input
          ref={(input) => {
            this.refFocus = input;
          }}
          defaultValue={task}
          readOnly={this.state.edit}
          type="text"
          style={style}
        />

        <ButtonsAll
          handleImportant={this.handleImportant}
          handleEdit={this.handleEdit}
          edit={this.state.edit}
        />
      </div>
    );
  }
}

export default SingleTask;
