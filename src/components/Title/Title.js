import React, { createElement } from "react";
import styles from "./Title.module.css";

class Title extends React.Component {
  render() {
    const { title,tagName } = this.props
    return (
      <div className={styles.title}>
        {title}
        <div className={styles.gradient}></div>
      </div>
    );
  }
}

export default Title;
