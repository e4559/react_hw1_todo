import React from "react";
import Title from "../Title";
import styles from "./AddTasks.module.css";

class AddTasks extends React.Component {
  render() {
    return (
      <div className={styles.container}>
        <Title title={<h1>My Tasks</h1>} />
        <div className={styles.inputWrapper}>
          <input className={styles.input} placeholder="Add Task" />
          <button>ADD TASK</button>
        </div>
      </div>
    );
  }
}

export default AddTasks;
